# Connectors

This folder contains packages that contain simple interfaces to interact with the services used by the Swiss Data Custodian. Currently, we have connectors for
- Kafka
- Pulsar
- MongoDB

## Message Queue Connectors

The `mqconnector` package contains two interfaces: `MQProducer` and `MQConsumer`.

### MQProducer

The MQProducer interface contains the `SendMessage` function. It sends a message to the message queue.

### MQConsumer

The MQConsumer interface contains the `SubscribeAndConsume` function. It subscribes to a message queue and consumes its messages. When a message is consumed, a corresponding function is called.

## Kafka and Pulsar

`KafkaConnector` and `PulsarConnector` are two structures implementing the `MQProducer` and `MQConsumer` interfaces. They can be used to send and receive messages from Kafka and Pulsar message queues.

### KafkaConnector
The KafkaConnector struct contains two attribues:
* `URL`: the URL of the Kafka message queue.
* `WriteDeadLine`: the deadline for future Write calls and any currently-blocked Write call

### PulsarConnector
The PulsarConnector struct also contains two attributes:
* `URL`: the URL of the Pulsar message queue
* `ConsumeAndDelete`: a boolean specifying if messages must be removed from the message queue once they have been read.


## MongoDB Functions

The `mongofunc` package provides the `MongoConnection` struct which can be used to simplify interaction with MongoDB.

### MongoConnection

The `MongoConnection` struct has four attributes:
* `MongoURL`: the URL of the mongo DB instance
* `Database`: the name of the database
* `Collection`: the name of the collection
* `client`: the client created to communicate with the database

## Documentation

To read the documentation you must install `godoc`. Open a terminal in the current folder and run the following command:

```
godoc -http=localhost:6060
```

The documentation for the MongoDB functions is accessible at [http://localhost:6060/pkg/gitlab.com/data-custodian/custodian-go/connectors/](http://localhost:6060/pkg/gitlab.com/data-custodian/custodian-go/connectors/)
