/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mqconnector

import (
	"context"
	"log"
	"time"

	"github.com/apache/pulsar-client-go/pulsar"
)

type PulsarConnector struct {
	URL              string //the URL of the Pulsar message queue
	ConsumeAndDelete bool   // when set to true, read messages are instantly removed from the message queue
}

// SendMessage sends *message* with *key* to the Pulsar message queue under the *topic* topic
func (c PulsarConnector) SendMessage(ctx context.Context, message []byte, key string, topic string, properties map[string]string) error {
	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL: c.URL,
	})
	if err != nil {
		return err
	}

	defer client.Close()

	producer, err := client.CreateProducer(pulsar.ProducerOptions{
		Topic: topic,
	})

	if err != nil {
		return err
	}

	_, err = producer.Send(ctx, &pulsar.ProducerMessage{
		Payload:    message,
		Key:        key,
		EventTime:  time.Now(),
		Properties: properties,
	})

	defer producer.Close()

	if err != nil {
		return err
	} else {
		return nil
	}
}

// SubscribeAndConsume subscribed to *topic* in the the Pulsar message queue as *subscriptionName*. When a message is received, it checks its key and looks in *commands*
// which function must be executed.
func (c PulsarConnector) SubscribeAndConsume(ctx context.Context, topic string, subscriptionName string, commands map[string]CommandHandler, maxRetry int, sleepTime time.Duration) {
	msgChannel := make(chan pulsar.ConsumerMessage)

	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL: c.URL,
	})

	if err != nil {
		log.Fatalf("Could not create client: %v", err)
	}

	config := pulsar.ConsumerOptions{
		Topic:            topic,
		SubscriptionName: subscriptionName,
		Type:             pulsar.Exclusive,
		MessageChannel:   msgChannel,
	}

	var consumer pulsar.Consumer = nil

	for i := 0; i < maxRetry; i++ {
		consumer, err = client.Subscribe(config)

		if err != nil {
			log.Println("Could not establish subscription. Retrying in " + sleepTime.String())
			time.Sleep(sleepTime)
		} else {
			break
		}
	}

	if consumer == nil {
		log.Fatalf("Could not establish subscription: %v", err)
	}

	defer consumer.Close()

	for cm := range msgChannel {
		msg := cm.Message

		// Process the message
		log.Println(msg.Key(), msg.Payload())

		if command, ok := commands[msg.Key()]; ok {
			err := command(ctx, msg.Payload())
			if err != nil {
				log.Println("Error while processing message:", err)
			}
		} else {
			log.Println("Error: malformed command")
		}

		if c.ConsumeAndDelete {
			consumer.Ack(msg)
		}
	}
}
