/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mqconnector

import (
	"context"
)

type CommandHandler func(context.Context, []byte) error

type MQProducer interface {
	SendMessage(context.Context, []byte, string, string, map[string]string) error // SendMessage(context, message, key, topic, properties)
}

type MQConsumer interface {
	SubscribeAndConsume(ctx context.Context, topic string, subscriptionName string, commands map[string]CommandHandler, maxRetry int, sleep int)
}
