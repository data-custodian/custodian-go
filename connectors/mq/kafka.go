/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mqconnector

import (
	"context"
	"log"
	"time"

	"github.com/segmentio/kafka-go"
)

type KafkaConnector struct {
	URL           string        //the URL of the Kafka message queue
	WriteDeadline time.Duration // the deadline for future Write calls and any currently-blocked Write call
}

func (c KafkaConnector) produceMessage(ctx context.Context, message []byte, key string, topic string, partition int) error {

	conn, err := kafka.DialLeader(ctx, "tcp", c.URL, topic, partition)

	if err != nil {
		return err
	}

	conn.SetWriteDeadline(time.Now().Add(c.WriteDeadline))
	conn.WriteMessages(
		kafka.Message{Key: []byte(key), Value: message},
	)

	conn.Close()
	return nil
}

// SendMessage sends *message* with *key* to the Kafka message queue under the specified *topic*.
// Currently, you cannot choose to which partition you send the message (it automatically sends to partition 0).
func (c KafkaConnector) SendMessage(ctx context.Context, message []byte, key string, topic string, properties map[string]string) error {
	// TODO: how to choose partition?
	// FIXME: add properties to the message
	partition := 0
	err := c.produceMessage(ctx, message, key, topic, partition)
	if err != nil {
		return err
	}
	return nil
}

func (c KafkaConnector) SubscribeAndConsume(ctx context.Context, topic string, subscriptionName string, commands map[string]CommandHandler) {
	// make a new reader that consumes from topic-A
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{c.URL},
		GroupID:  subscriptionName,
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})

	for {
		msg, err := r.ReadMessage(ctx)
		if err != nil {
			break
		}
		log.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", msg.Topic, msg.Partition, msg.Offset, string(msg.Key), string(msg.Value))

		if command, ok := commands[string(msg.Key)]; ok {
			err := command(ctx, msg.Value)
			if err != nil {
				log.Println("Error while processing message:", err)
			}
		} else {
			log.Fatal("Error: malformed command")
		}
	}

	r.Close()
}
