/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package mongofunc

import (
	"context"
	"log"

	"gitlab.com/data-custodian/custodian-go/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	MongoURL   string
	Database   string
	Collection string

	client *mongo.Client
}

// NewMongoDB creates a new [MongoConnection] object
func NewMongoDB(mongoURL, database, collection string) MongoConnection {
	return MongoConnection{mongoURL, database, collection, nil}
}

// mongoDBConnect establishes the connection to the mongo DB
func (m *MongoConnection) mongoDBConnect(ctx context.Context) error {

	// Set client options
	clientOptions := options.Client().ApplyURI(m.MongoURL)

	// Connect to MongoDB
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return err
	}
	log.Println("Connected to MongoDB")

	m.client = client

	return nil
}

// CloseClientDB closes the connection to the mongo DB
func (m *MongoConnection) CloseClientDB(ctx context.Context) {
	if m.client == nil {
		return
	}

	err := m.client.Disconnect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Connection to MongoDB closed.")
}

// GetAsset gets the asset with corresponding OID from the database, if it exists, and stores it in asset
func (m *MongoConnection) GetAsset(ctx context.Context, OID string, asset interface{}) error {

	return m.GetAssetByField(ctx, "id", OID, asset)
}

// GetAssetByField gets the asset with corresponding field value from the database, if it exists, and stores it in asset
func (m *MongoConnection) GetAssetByField(ctx context.Context, field string, value string, asset interface{}) error {
	attributesMap := make(map[string]interface{})
	attributesMap[field] = value
	return m.GetAssetByFields(ctx, attributesMap, asset, options.FindOne())
}

// GetAssetsByFields gets all assets with corresponding field values from the database, if they exist, and sorts them by field
func (m *MongoConnection) GetAssetsByFieldsSorted(ctx context.Context, attributesMap map[string]interface{}, field string, order int) (*mongo.Cursor, error) {
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{field, order}})
	//return m.GetAssetByFields(ctx, attributesMap, asset, findOptions)

	filter := bson.M{} // FIXME: DRY: use GetAssetsByFilterSorted

	for field, value := range attributesMap {
		filter[field] = value
	}

	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return nil, err
		}
	}

	collection := m.client.Database(m.Database).Collection(m.Collection)

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		return nil, err
	} else {
		return cursor, nil
	}
}

// GetAssetsByFilterSorted gets all assets with corresponding filter from the database, if they exist, and sorts them by field
func (m *MongoConnection) GetAssetsByFilterSorted(ctx context.Context, filter bson.M, field string, order int) (*mongo.Cursor, error) {
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{field, order}})

	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return nil, err
		}
	}

	collection := m.client.Database(m.Database).Collection(m.Collection)

	cursor, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		return nil, err
	} else {
		return cursor, nil
	}
}

// GetAssetByFields gets the asset with corresponding field values from the database, if it exists, and stores it in asset
func (m *MongoConnection) GetAssetByFields(ctx context.Context, attributesMap map[string]interface{}, asset interface{}, findOptions *options.FindOneOptions) error {
	filter := bson.M{}

	for field, value := range attributesMap {
		filter[field] = value
	}

	return m.GetAssetByFilter(ctx, filter, asset, findOptions)
}

// GetAssetByFilter gets the asset with corresponding filter from the database, if it exists, and stores it in asset
func (m *MongoConnection) GetAssetByFilter(ctx context.Context, filter bson.M, asset interface{}, findOptions *options.FindOneOptions) error {

	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return err
		}
	}

	collection := m.client.Database(m.Database).Collection(m.Collection)

	err := collection.FindOne(ctx, filter, findOptions).Decode(asset.(interface{}))
	if err != nil {
		return err
	}

	return nil
}

// GetAssetByField returns all assets with corresponding field value from the database, if it exists
func (m *MongoConnection) GetAssetsByField(ctx context.Context, field string, value string) (*mongo.Cursor, error) {
	attributesMap := make(map[string]interface{})
	attributesMap[field] = value
	return m.GetAssetsByFields(ctx, attributesMap, options.Find())
}

// Returns multiple assets corresponding to the fields
func (m *MongoConnection) GetAssetsByFields(ctx context.Context, attributesMap map[string]interface{}, findOptions *options.FindOptions) (*mongo.Cursor, error) {

	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return nil, err
		}
	}

	collection := m.client.Database(m.Database).Collection(m.Collection)

	filter := bson.M{}

	for field, value := range attributesMap {
		filter[field] = value
	}

	cur, err := collection.Find(ctx, filter, findOptions)
	if err != nil {
		return nil, err
	}

	return cur, nil
}

// Exists checks if a asset with corresponding OID exists in the database
func (m *MongoConnection) Exists(ctx context.Context, OID string) (bool, error) {
	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return false, err
		}
	}

	collection := m.client.Database(m.Database).Collection(m.Collection)

	var result interface{}

	filter := bson.D{{"id", OID}}

	err := collection.FindOne(ctx, filter).Decode(&result)

	if err != nil {
		return false, nil
	} else {
		return true, nil
	}
}

// UpdateAsset updates the asset with corresponding OID using data
func (m *MongoConnection) UpdateAsset(ctx context.Context, OID string, data interface{}) error {

	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return err
		}
	}

	collection := m.client.Database(m.Database).Collection(m.Collection)

	filter := bson.M{"id": bson.M{"$eq": OID}}

	// We convert the data to update into BSON
	updateData, err := bson.Marshal(data)
	if err != nil {
		return err
	}

	// We translate the update to bson.M
	var updatePayload bson.M

	err = bson.Unmarshal(updateData, &updatePayload)
	if err != nil {
		return err
	}

	// The asset update
	update := bson.M{"$set": updatePayload}

	updateResult, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	log.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)
	return nil
}

// AddAsset adds a new asset to the database
func (m *MongoConnection) AddAsset(ctx context.Context, OID string, asset custodian.Asset) error {

	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return err
		}
	}

	collection := m.client.Database(m.Database).Collection(m.Collection)

	asset.SetOID(OID)

	insertResult, err := collection.InsertOne(ctx, asset)

	if err != nil {
		return err
	}

	log.Println("Inserted a single document: ", insertResult.InsertedID)

	return nil
}

// EraseCollection erases every documents in a collection
func (m *MongoConnection) EraseCollection(ctx context.Context) error {

	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return err
		}
	}

	coll := m.client.Database(m.Database).Collection(m.Collection)

	result, err := coll.DeleteMany(ctx, bson.D{})
	if err != nil {
		return err
	}

	log.Println(result)

	return nil
}

// UpdateAsset updates the asset with corresponding OID using data
func (m *MongoConnection) DeleteAsset(ctx context.Context, OID string) error {

	if m.client == nil {
		err := m.mongoDBConnect(ctx)
		if err != nil {
			return err
		}
	}

	collection := m.client.Database(m.Database).Collection(m.Collection)

	// filter posts tagged as golang
	filter := bson.M{"id": OID}

	// find one document per OID

	_, err := collection.DeleteOne(ctx, filter)
	if err != nil {
		return err

	}
	return nil
}
