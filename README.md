# custodian-go

## Installation

1. To install the Custodian Go package, you need to have [Go](https://go.dev/doc/install) installed. This module was tested using Go version `go1.20.1`.
2. You can then add the module as a dependency in your program with `go get`.
```sh
go get -u gitlab.com/data-custodian/custodian-go
```
3. Import the module in your code
```go
import "gitlab.com/data-custodian/custodian-go"
```

## Usage

You can generate the documentation locally using GoDoc. Open a terminal in the root folder of this repo and run the following command
```sh
godoc -http=localhost:6060
```

  The documentation will then be accessible at [http://localhost:6060/pkg/gitlab.com/data-custodian/custodian-go/](http://localhost:6060/pkg/gitlab.com/data-custodian/custodian-go/).

## Code of Conduct

SDC is a community-driven open source project. The SDC leadership has made a strong commitment to creating an open, inclusive, and positive community. Please read the [SDC Code of Conduct](https://sabrinaossey.github.io/Custodian-doc/starting/codeofconduct/) for guidance on how to interact with others in a way that makes our community thrive.


## How to Contribute

We deeply appreciate your interest in contributing to the Swiss Data Custodian Project. Here’s a step-by-step guide to assist you through the process of contributing:

### Contributor License Agreement Guidance

Before making contributions, ensure you have signed the appropriate Contributor License Agreement (CLA).

- **Individual Contributors (CLAI):** If you're contributing independently (not on behalf of a company), sign the [Individual Contributor License Agreement (CLAI)](https://gitlab.com/data-custodian/custodian/-/blob/develop/CLAI.txt).
- **Corporate Contributors (CLAC):** If contributions are made by employees on behalf of a company, the company should sign the [Corporate Contributor License Agreement (CLAC)](https://gitlab.com/data-custodian/custodian/-/blob/develop/CLAC.txt).

### Contribution Steps

For minor improvements, consider opening a new issue. For larger contributions or if you want to discuss privately before contributing, contact our coordinators at [contact@datascience.ch](mailto:contact@datascience.ch).

### Follow Contribution Guidelines

For more details on contributing, please visit our [Contribution Guide](https://sdsc-ord.github.io/Custodian_documentation/starting/contributing/) on the project's website.
  
Thank you for your interest and support! Happy contributing!

  
## License Information
  
Copyright © 2019-2023 Swiss Data Science Center, [www.datascience.ch](https://www.datascience.ch/). All rights reserved.

The Swiss Data Custodian software is distributed as open-source under the AGPLv3 license or any later version. Details about the license can be found in the `LICENSE.AGPL` file included within the distribution package.

If the AGPLv3 license does not accommodate your project or business needs, alternative licensing options are available to meet specific requirements. These arrangements can be facilitated through the EPFL Technology Transfer Office. For more information, kindly visit their official website at [tto.epfl.ch](https://tto.epfl.ch/) or direct your inquiries via email to [info.tto@epfl.ch](mailto:info.tto@epfl.ch).

Please note that this software should not be used to deliberately harm any individual or entity. Users and developers must adhere to ethical guidelines and use the software responsibly and legally.
