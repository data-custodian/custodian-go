package util

import (
	b64 "encoding/base64"
	"testing"
	"time"

	custodian "gitlab.com/data-custodian/custodian-go/models"
)

var signerContract = custodian.ContractBody{
	"sampleContractID",
	[]custodian.Tag{custodian.Tag{"key", "value"}},
	[]custodian.ContractDescription{custodian.ContractDescription{"en", "title", "description", "check", []string{"accept"}}},
	[]custodian.Variable{custodian.Variable{"id", "value", "type", "regexp"}},
	[]custodian.Subject{custodian.Subject{"id", map[string]interface{}{"key": "value"}}},
	[]custodian.Verb{custodian.Verb{"id", "function", map[string]interface{}{"key": "value"}}},
	[]custodian.Object{custodian.Object{"id", "owner", map[string]interface{}{"key": "value"}}},
	[]custodian.Scope{custodian.Scope{[]string{"subject"}, []string{"verb"}, []string{"object"}}},
	custodian.Validity{time.Now(), time.Now().Add(time.Hour * 24 * 365), []string{"subject"}},
}

func TestAddSeal(t *testing.T) {
	// addSeal(signature custodian.Signature, ecdsaKey *ecdsa.PrivateKey, userid string, source string)
	userID := "userid"
	source := "source"

	privateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}

	signature := custodian.Signature{}
	nextSignature, err := addSeal(signature, nil, userID, source)
	if err == nil {
		t.Error("Signature sealing succeeded with nil key")
	}

	nextSignature, err = addSeal(signature, privateKey, "", source)
	if err == nil {
		t.Error("Signature sealing succeeded with empty user ID")
	}

	nextSignature, err = addSeal(signature, privateKey, userID, "")
	if err == nil {
		t.Error("Signature sealing succeeded with empty source")
	}

	nextSignature, err = addSeal(signature, privateKey, userID, source)
	if err != nil {
		t.Fatal(err)
	} else if len(nextSignature.Seal) != 1 {
		t.Errorf("Signature sealing failed: contains %d seals instead of 1", len(nextSignature.Seal))
	} else if nextSignature.Seal[0].Signee != userID {
		t.Errorf("Signature sealing failed: signee is '%s' instead of '%s'", nextSignature.Seal[0].Signee, userID)
	}

}

func TestSignContract(t *testing.T) {
	privateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}

	signature, err := SignContract(signerContract, privateKey, "userid", "source")
	if err != nil {
		t.Fatal(err)
	}
	if signature.Header.Prev != (custodian.Link{}) {
		t.Errorf("Signature sealing failed: previous signature is not nil")
	}

	if len(signature.Seal) != 1 {
		t.Errorf("Signature sealing failed: contains %d seals instead of 1", len(signature.Seal))
	}

	if signature.Header.Body.Digest != b64.StdEncoding.EncodeToString(signerContract.Hash()) {
		t.Errorf("Signature sealing failed: body digest is '%s' instead of '%s'", signature.Header.Body.Digest, b64.StdEncoding.EncodeToString(signerContract.Hash()))
	}

	if ok, err := VerifyLastContractSignature(signature, &privateKey.PublicKey); err != nil || !ok {
		t.Error("Signature verification failed: returned false for correct key")
	}
}

func TestSignSignature(t *testing.T) {
	userID := "userid"
	source := "source"

	privateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}

	firstSignature, err := SignContract(signerContract, privateKey, userID, source)
	if err != nil {
		t.Fatal(err)
	}

	secondSignature, err := SignSignature(firstSignature, privateKey, userID, source)
	if err != nil {
		t.Fatal(err)
	}

	if len(secondSignature.Seal) != 2 {
		t.Errorf("Signature sealing failed: contains %d seals instead of 1", len(secondSignature.Seal))
	}

	lastSeal := secondSignature.Seal[len(secondSignature.Seal)-1]
	if lastSeal.Signee != userID {
		t.Errorf("Signature sealing failed: signee is '%s' instead of '%s'", lastSeal.Signee, userID)
	}
	if lastSeal.Source != source {
		t.Errorf("Signature sealing failed: source is '%s' instead of '%s'", lastSeal.Source, source)
	}
	if lastSeal.SignatureMethod != "ECDSA" {
		t.Errorf("Signature sealing failed: algorithm is '%s' instead of 'ECDSA'", lastSeal.SignatureMethod)
	}

	firstSignatureHash, err := firstSignature.Hash()
	if err != nil {
		t.Fatal(err)
	}

	if secondSignature.Header.Prev.Digest != b64.StdEncoding.EncodeToString(firstSignatureHash) {
		t.Errorf("Signature sealing failed: previous digest is '%s' instead of '%s'", secondSignature.Header.Prev.Digest, b64.StdEncoding.EncodeToString(firstSignatureHash))
	}
	if secondSignature.Header.Prev.ID != firstSignature.OID {
		t.Errorf("Signature sealing failed: previous id is '%s' instead of '%s'", secondSignature.Header.Prev.ID, firstSignature.OID)
	}

	if ok, err := VerifyLastContractSignature(secondSignature, &privateKey.PublicKey); err != nil || !ok {
		t.Error("Signature verification failed: returned false for correct key")
	}

}

/*
SignContractUpdate(contractSignature custodian.Signature, userid string, ecdsaKey *ecdsa.PrivateKey, source string, newContract *custodian.ContractBody) (custodian.Signature, error)
*/

func TestSignContractUpdate(t *testing.T) {
	privateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}

	signature, err := SignContract(signerContract, privateKey, "userid", "source")
	if err != nil {
		t.Fatal(err)
	}
	newContract := signerContract
	newContract.OID = "newID"
	newSignature, err := SignContractUpdate(signature, "userid", privateKey, "source", &newContract)
	if err != nil {
		t.Fatal(err)
	}
	if newSignature.Header.Prev.ID != signature.OID {
		t.Errorf("Signature sealing failed: previous signature is '%s' instead of '%s'", newSignature.Header.Prev.ID, signature.OID)
	}

	signatureHash, err := signature.Hash()
	if err != nil {
		t.Fatal(err)
	}

	if newSignature.Header.Prev.Digest != b64.StdEncoding.EncodeToString(signatureHash) {
		t.Errorf("Signature sealing failed: previous signature digest is '%s' instead of '%s'", newSignature.Header.Prev.Digest, b64.StdEncoding.EncodeToString(signatureHash))
	}

	if newSignature.Header.Body.Digest != b64.StdEncoding.EncodeToString(newContract.Hash()) {
		t.Errorf("Signature sealing failed: body digest is '%s' instead of '%s'", newSignature.Header.Body.Digest, b64.StdEncoding.EncodeToString(newContract.Hash()))
	}

	if ok, err := VerifyLastContractSignature(newSignature, &privateKey.PublicKey); err != nil || !ok {
		t.Error("Signature verification failed: returned false for correct key")
	}
}
