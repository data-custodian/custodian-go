package util

import (
	"errors"
	"os"
	"strings"
	"testing"
)

func TestNewECDSAKey(t *testing.T) {
	privateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	} else if privateKey == nil {
		t.Fatal("Private key is nil")
	} else if privateKey.Public() == nil {
		t.Fatal("Public key is nil")
	}
}

func pemGetLabel(pemString string) (string, error) {
	lines := strings.Split(pemString, "-----")
	if len(lines) < 5 {
		return "", errors.New("Invalid PEM string")
	}
	beginLine := lines[1]
	endLine := lines[3]
	label := strings.TrimPrefix(beginLine, "BEGIN ")
	if label != strings.TrimPrefix(endLine, "END ") {
		return "", errors.New("Invalid PEM string")
	}
	return label, nil
}

// TestECDSAEncodeToPEM tests the ecdsaEncodeToPEM function
func TestECDSAEncodeToPEM(t *testing.T) {
	privateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}
	privPEM, pubPEM, err := ecdsaEncodeToPEM(privateKey)
	if err != nil {
		t.Fatal(err)
	}
	if privPEM == "" {
		t.Fatal("Private key PEM is empty")
	}
	if pubPEM == "" {
		t.Fatal("Public key PEM is empty")
	}
	label, err := pemGetLabel(privPEM)
	if err != nil {
		t.Fatal(err)
	}
	if label != "PRIVATE KEY" {
		t.Fatal("Invalid label for private key: got " + label + " instead of PRIVATE KEY")
	}
	label, err = pemGetLabel(pubPEM)
	if err != nil {
		t.Fatal(err)
	}
	if label != "PUBLIC KEY" {
		t.Fatal("Invalid label for public key: got " + label + " instead of PUBLIC KEY")
	}
}

// TestWriteAndReadECDSAKey tests the WriteECDSAKey, ReadECDSAPrivateKey and ReadECDSAPublicKey functions. This also tests the decodeECDSAPrivateKey and decodeECDSAPublicKey functions.
func TestWriteAndReadECDSAKey(t *testing.T) {
	privateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}

	// Create a temporary directory
	tempDir, err := os.MkdirTemp("", "custodian")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tempDir)

	keysPath := tempDir + "/ecdsa"
	err = WriteECDSAKeys(keysPath, privateKey)
	if err != nil {
		t.Fatal(err)
	}

	loadedPrivateKey, err := ReadECDSAPrivateKey(keysPath + ".priv")
	if err != nil {
		t.Fatal(err)
	} else if loadedPrivateKey == nil {
		t.Fatal("Private key is nil")
	}
	loadedPublicKey, err := ReadECDSAPublicKey(keysPath + ".pub")
	if err != nil {
		t.Fatal(err)
	} else if loadedPublicKey == nil {
		t.Fatal("Public key is nil")
	}
	if !loadedPrivateKey.Equal(privateKey) {
		t.Fatal("Generated public key and saved/loaded private key are not equal", privateKey, loadedPrivateKey)
	}
	if !loadedPublicKey.Equal(privateKey.Public()) {
		t.Fatal("Generated public key and saved/loaded public key are not equal")
	}

}
