package util

import (
	"gitlab.com/data-custodian/custodian-go/models"
	"os"
	"testing"
	"time"
)

var ioContract = custodian.ContractBody{
	"sampleID",
	[]custodian.Tag{custodian.Tag{"key", "value"}},
	[]custodian.ContractDescription{custodian.ContractDescription{"en", "title", "description", "check", []string{"accept"}}},
	[]custodian.Variable{custodian.Variable{"id", "value", "type", "regexp"}},
	[]custodian.Subject{custodian.Subject{"id", map[string]interface{}{"key": "value"}}},
	[]custodian.Verb{custodian.Verb{"id", "function", map[string]interface{}{"key": "value"}}},
	[]custodian.Object{custodian.Object{"id", "owner", map[string]interface{}{"key": "value"}}},
	[]custodian.Scope{custodian.Scope{[]string{"subject"}, []string{"verb"}, []string{"object"}}},
	custodian.Validity{time.Now(), time.Now().Add(time.Hour * 24 * 365), []string{"subject"}},
}
var ioSignature = custodian.Signature{
	"sampleID",
	custodian.SignatureHeader{
		custodian.Link{
			"sampleID",
			"SHA256base64",
			"c2FtcGxlSGFzaAo=",
		},
		custodian.Link{
			"sampleID",
			"SHA256base64",
			"c2FtcGxlSGFzaAo=",
		},
	},
	[]custodian.Seal{
		custodian.Seal{
			"signee",
			time.Now(),
			"source",
			"ECDSA",
			"signature",
		},
	},
}

func TestWriteAndReadContract(t *testing.T) {
	// Create a temporary directory
	tempDir, err := os.MkdirTemp("", "custodian")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tempDir)

	filename := tempDir + "/contract.json"

	// Write the contract to a file
	WriteContract(filename, ioContract)
	// Read the contract from the file
	loadedContract, err := ReadContract(filename)
	if err != nil {
		t.Error("Error while reading contract from file:", err)
	}
	// Compare the contract read from the file with the original contract
	if ioContract.String() != loadedContract.String() {
		t.Error("Contract read from file is not the same as the original contract")
	}
}

func TestWriteAndReadSignature(t *testing.T) {
	// Create a temporary directory
	tempDir, err := os.MkdirTemp("", "custodian")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tempDir)

	filename := tempDir + "/signature.json"

	// Write the signature to a file
	WriteSignature(filename, ioSignature)
	// Read the signature from the file
	loadedSignature, err := ReadSignature(filename)
	if err != nil {
		t.Error("Error while reading signature from file:", err)
	}
	// Compare the signature read from the file with the original signature
	if ioSignature.String() != loadedSignature.String() {
		t.Error("Signature read from file is not the same as the original signature")
	}
}
