/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package util

import (
	//"context"
	"crypto/ecdsa"
	b64 "encoding/base64"
	"errors"
	"log"

	custodian "gitlab.com/data-custodian/custodian-go/models"
)

// VerifyHashSignature signs `hash` with `publicKey` and compares it to `signedHashB64`. It returns true if the signature is correct, and false otherwise.
func VerifyHashSignature(signedHashB64 string, hash []byte, publicKey *ecdsa.PublicKey) (bool, error) {
	// Signs the hash and checks the difference
	log.Println("Public key:", publicKey)
	log.Println("Signed Hash:", signedHashB64)

	if publicKey == nil {
		return false, errors.New("Public key is nil")
	}

	signedHash, err := b64.StdEncoding.DecodeString(signedHashB64)
	if err != nil {
		return false, err
	}

	verified := ecdsa.VerifyASN1(publicKey, hash, signedHash)

	return verified, nil
}

// VerifyLastContractSignature varifies that the last signature of the seal array is correct. It returns true if the signature is correct, and false otherwise.
func VerifyLastContractSignature(contractSignature custodian.Signature, publicKey *ecdsa.PublicKey) (bool, error) {
	//TODO: check that all needed signatures are here

	log.Println("Verification of signature", contractSignature.OID)

	seal_i := len(contractSignature.Seal) - 1
	seal := contractSignature.Seal[seal_i]
	signedHashB64 := seal.Signature

	hash, err := contractSignature.PreSignatureHash()
	if err != nil {
		return false, err
	}

	log.Println("Pre-signature hash:", b64.StdEncoding.EncodeToString(hash))

	isValid, err := VerifyHashSignature(signedHashB64, hash, publicKey)
	if err != nil {
		return false, err
	}

	return isValid, nil
}
