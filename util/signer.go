/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package util

import (
	"crypto/ecdsa"
	"crypto/rand"
	b64 "encoding/base64"
	"errors"
	"fmt"
	"time"

	custodian "gitlab.com/data-custodian/custodian-go/models"
)

// SignSignature signs a signature object and returns the next signature in the chain.
func SignSignature(signature custodian.Signature, ecdsaKey *ecdsa.PrivateKey, userid string, source string) (custodian.Signature, error) {
	if len(signature.Seal) < 1 {
		return custodian.Signature{}, errors.New("Cannot sign a signature without a seal")
	}

	timestamp := time.Now().Round(time.Millisecond)

	// Update the signature header with the previous signature's hash

	previousSignatureHash, err := signature.Hash()
	if err != nil {
		fmt.Println("Error:", err)
		return custodian.Signature{}, err
	}

	signature.Header.Prev = custodian.Link{signature.OID, "SHA256", b64.StdEncoding.EncodeToString(previousSignatureHash)}

	// Set a new OID for the signature

	newOID := signature.ComputeOID(timestamp, userid)
	signature.SetOID("did:custodian:" + newOID)

	// Add a new seal to the signature
	signature, err = addSeal(signature, ecdsaKey, userid, source)
	if err != nil {
		return custodian.Signature{}, err
	}

	return signature, nil
}

func addSeal(signature custodian.Signature, ecdsaKey *ecdsa.PrivateKey, userid string, source string) (custodian.Signature, error) {
	if ecdsaKey == nil {
		return custodian.Signature{}, errors.New("No ECDSA key provided")
	} else if userid == "" {
		return custodian.Signature{}, errors.New("No user ID provided")
	} else if source == "" {
		return custodian.Signature{}, errors.New("No source provided")
	}

	timestamp := time.Now().Round(time.Millisecond) // We use a rounded timestamp to make sure no information is lost when storing in the DB. Otherwise, hashes (and signatures) could mismatch

	newSeal := custodian.Seal{userid, timestamp, source, "ECDSA", ""}

	signatureCopy := signature

	signatureCopy.Seal = append(signature.Seal, newSeal)

	signatureHash, err := signatureCopy.Hash()
	if err != nil {
		return custodian.Signature{}, err
	}

	signedHash, err := ecdsa.SignASN1(rand.Reader, ecdsaKey, signatureHash)
	if err != nil {
		return custodian.Signature{}, err
	}

	newSeal.Signature = b64.StdEncoding.EncodeToString(signedHash)
	signature.Seal = append(signature.Seal, newSeal)

	return signature, nil
}

// SignContract signs a contract with the given ECDSA key and user ID
func SignContract(contract custodian.ContractBody, ecdsaKey *ecdsa.PrivateKey, userid string, source string) (custodian.Signature, error) {
	timestamp := time.Now().Round(time.Millisecond)

	var signatureTemplate custodian.Signature
	signatureTemplate.Header.Body.ID = contract.OID
	signatureTemplate.Header.Body.DigestMethod = "SHA256"
	signatureTemplate.Header.Body.Digest = b64.StdEncoding.EncodeToString(contract.Hash())

	// Set a new OID for the signature
	newOID := signatureTemplate.ComputeOID(timestamp, userid)
	signatureTemplate.SetOID("did:custodian:" + newOID)

	// Add a new seal to the signature
	contractSignature, err := addSeal(signatureTemplate, ecdsaKey, userid, source)
	if err != nil {
		return custodian.Signature{}, err
	}

	return contractSignature, nil
}

// SignContractUpdate adds a new signature to the chain with a link to the updated contract.
// The updated contract is created using the previous contract as a template and must follow its design
// - It must have the same fields as the previous contract
// - It must have the same values for all fields except the ones that were designed to be updated
// - The updated fields must follow the regular expression defined in the contract
func SignContractUpdate(contractSignature custodian.Signature, userid string, ecdsaKey *ecdsa.PrivateKey, source string, newContract *custodian.ContractBody) (custodian.Signature, error) {
	if newContract == nil {
		return custodian.Signature{}, errors.New("No contract provided")
	}

	timestamp := time.Now().Round(time.Millisecond)

	signatureHash, err := contractSignature.Hash()
	if err != nil {
		fmt.Println("Error:", err)
		return custodian.Signature{}, err
	}

	// We update the signature

	if len(contractSignature.Seal) > 0 {
		contractSignature.Header.Prev = custodian.Link{contractSignature.OID, "SHA256", b64.StdEncoding.EncodeToString(signatureHash)}
	}
	contractSignature.Header.Body = custodian.Link{newContract.OID, "SHA256", b64.StdEncoding.EncodeToString(newContract.Hash())}

	// Set a new OID for the signature

	newOID := contractSignature.ComputeOID(timestamp, userid)
	contractSignature.SetOID("did:custodian:" + newOID)

	// Add a new seal to the signature
	signature, err := addSeal(contractSignature, ecdsaKey, userid, source)
	if err != nil {
		return custodian.Signature{}, err
	}

	return signature, nil
}

// FIXME: For debugging only, to remove later
// DebugSignContract does the same as SignContract, but lets you predefine the OID of the new signature.
func DebugSignContract(contract custodian.ContractBody, ecdsaKey *ecdsa.PrivateKey, userid string, source string, signatureOID string) (custodian.Signature, error) {
	var signatureTemplate custodian.Signature
	signatureTemplate.Header.Body.ID = contract.OID
	signatureTemplate.Header.Body.Digest = b64.StdEncoding.EncodeToString(contract.Hash())

	contractSignature, err := DebugSignSignature(signatureTemplate, ecdsaKey, userid, source, signatureOID)
	if err != nil {
		return custodian.Signature{}, err
	}
	return contractSignature, nil
}

// FIXME: For debugging only, to remove later
// SignSignature signs a signature object and returns the next signature in the chain.
func DebugSignSignature(signature custodian.Signature, ecdsaKey *ecdsa.PrivateKey, userid string, source string, signatureOID string) (custodian.Signature, error) {
	// Update the signature header with the previous signature's hash
	previousSignatureHash, err := signature.Hash()
	if err != nil {
		fmt.Println("Error:", err)
		return custodian.Signature{}, err
	}

	if len(signature.Seal) > 0 {
		signature.Header.Prev = custodian.Link{signature.OID, "SHA256", b64.StdEncoding.EncodeToString(previousSignatureHash)}
	}

	// Set a new OID for the signature

	signature.SetOID("did:custodian:" + signatureOID)

	// Add a new seal to the signature
	signature, err = addSeal(signature, ecdsaKey, userid, source)
	if err != nil {
		return custodian.Signature{}, err
	}

	return signature, nil
}
