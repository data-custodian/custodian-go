package util

import (
	"encoding/json"
	"errors"
	"gitlab.com/data-custodian/custodian-go/models"
	"os"
)

func ReadContract(filename string) (custodian.ContractBody, error) {
	var contractBody custodian.ContractBody

	jsonFile, err := os.Open(filename)
	if err != nil {
		return custodian.ContractBody{}, err
	}
	defer jsonFile.Close()

	err = json.NewDecoder(jsonFile).Decode(&contractBody)
	if err != nil {
		return custodian.ContractBody{}, err
	}
	return contractBody, nil
}

func ReadSignature(filename string) (custodian.Signature, error) {
	var signature custodian.Signature

	jsonFile, err := os.Open(filename)
	if err != nil {
		return custodian.Signature{}, err
	}
	defer jsonFile.Close()

	err = json.NewDecoder(jsonFile).Decode(&signature)
	if err != nil {
		return custodian.Signature{}, err
	}

	err = signatureFormatCheck(signature)
	if err != nil {
		return custodian.Signature{}, err
	}

	return signature, nil
}

func WriteSignature(filename string, signature custodian.Signature) error {
	jsonFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer jsonFile.Close()

	err = json.NewEncoder(jsonFile).Encode(signature)
	if err != nil {
		return err
	}
	return nil
}

func WriteContract(filename string, contractBody custodian.ContractBody) error {
	jsonFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer jsonFile.Close()

	err = json.NewEncoder(jsonFile).Encode(contractBody)
	if err != nil {
		return err
	}
	return nil
}

func signatureFormatCheck(signature custodian.Signature) error {
	if signature.OID == "" {
		return errors.New("Signature ID is missing")
	}

	if signature.Header.Body.ID == "" {
		return errors.New("Signature body ID is missing")
	}

	if signature.Header.Body.DigestMethod == "" {
		return errors.New("Signature body digest method is missing")
	}

	if signature.Header.Body.Digest == "" {
		return errors.New("Signature body digest is missing")
	}

	if len(signature.Seal) == 0 {
		return errors.New("Signature seal is empty")
	}

	return nil
}
