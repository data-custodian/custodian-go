package util

import (
	"crypto/ecdsa"
	"crypto/rand"
	b64 "encoding/base64"
	"testing"
	"time"

	custodian "gitlab.com/data-custodian/custodian-go/models"
)

var verifierContract = custodian.ContractBody{
	"sampleContractID",
	[]custodian.Tag{custodian.Tag{"key", "value"}},
	[]custodian.ContractDescription{custodian.ContractDescription{"en", "title", "description", "check", []string{"accept"}}},
	[]custodian.Variable{custodian.Variable{"id", "value", "type", "regexp"}},
	[]custodian.Subject{custodian.Subject{"id", map[string]interface{}{"key": "value"}}},
	[]custodian.Verb{custodian.Verb{"id", "function", map[string]interface{}{"key": "value"}}},
	[]custodian.Object{custodian.Object{"id", "owner", map[string]interface{}{"key": "value"}}},
	[]custodian.Scope{custodian.Scope{[]string{"subject"}, []string{"verb"}, []string{"object"}}},
	custodian.Validity{time.Now(), time.Now().Add(time.Hour * 24 * 365), []string{"subject"}},
}

func TestVerifyHashSignature(t *testing.T) {
	goodPrivateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}
	wrongPrivateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}

	hash := []byte("test")
	signature, err := ecdsa.SignASN1(rand.Reader, goodPrivateKey, hash)
	if err != nil {
		t.Fatal(err)
	}

	b64Signature := b64.StdEncoding.EncodeToString(signature)

	if ok, err := VerifyHashSignature(b64Signature, hash, nil); err == nil || ok {
		t.Error("Signature verification failed: did not return any error for nil key")
	}

	if ok, err := VerifyHashSignature(b64Signature, hash, &goodPrivateKey.PublicKey); err != nil || !ok {
		t.Error("Signature verification failed: returned false for correct key")
	}

	if ok, err := VerifyHashSignature(b64Signature, hash, &wrongPrivateKey.PublicKey); err != nil || ok {
		t.Error("Signature verification failed: returned true for wrong key")
	}
}

func TestVerifyLastContractSignature(t *testing.T) {
	goodPrivateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}
	wrongPrivateKey, err := NewECDSAKey()
	if err != nil {
		t.Fatal(err)
	}

	signature, err := SignContract(verifierContract, goodPrivateKey, "userid", "source")
	if err != nil {
		t.Fatal(err)
	}

	if ok, err := VerifyLastContractSignature(signature, &goodPrivateKey.PublicKey); err != nil || !ok {
		t.Error("Signature verification failed: returned false for correct key")
	}

	if ok, err := VerifyLastContractSignature(signature, &wrongPrivateKey.PublicKey); err != nil || ok {
		t.Error("Signature verification failed: returned true for wrong key")
	}

}
