/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package util

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"
)

// ecdsaEncodeToPEM takes a ECDSA key pair and returns their PEM encoding
func ecdsaEncodeToPEM(privateKey *ecdsa.PrivateKey) (string, string, error) {
	x509Encoded, err := x509.MarshalECPrivateKey(privateKey)
	if err != nil {
		return "", "", err
	}

	pemEncoded := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Encoded})

	x509EncodedPub, err := x509.MarshalPKIXPublicKey(privateKey.Public())
	if err != nil {
		return "", "", err
	}
	pemEncodedPub := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub})

	return string(pemEncoded), string(pemEncodedPub), nil
}

// NewECDSAKey returns a new ECDSA key pair
func NewECDSAKey() (*ecdsa.PrivateKey, error) {
	curve := elliptic.P256()
	var privateKey = new(ecdsa.PrivateKey)
	privateKey, err := ecdsa.GenerateKey(curve, rand.Reader)
	if err != nil {
		return nil, err
	}
	return privateKey, nil
}

// decodeECDSAPrivateKey takes a PEM-encoded private ECDSA key and returns a private key with the *ecdsa.PrivateKey format
func decodeECDSAPrivateKey(pemEncoded []byte) (*ecdsa.PrivateKey, error) {
	block, _ := pem.Decode(pemEncoded)
	if block == nil {
		return nil, fmt.Errorf("failed to parse PEM block containing the key")
	}

	x509Encoded := block.Bytes
	privateKey, err := x509.ParseECPrivateKey(x509Encoded)
	if err != nil {
		return nil, err
	}

	return privateKey, nil
}

// DecodeECDSAPublicKeyFromPEM takes a PEM-encoded public ECDSA key and returns a public key with the *ecdsa.PublicKey format
func DecodeECDSAPublicKeyFromPEM(pemEncoded []byte) (*ecdsa.PublicKey, error) {
	block, _ := pem.Decode(pemEncoded)
	if block == nil {
		return nil, fmt.Errorf("failed to parse PEM block containing the key")
	}

	x509Encoded := block.Bytes
	genericPublicKey, err := x509.ParsePKIXPublicKey(x509Encoded)
	if err != nil {
		return nil, err
	}
	publicKey, ok := genericPublicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("failed to parse public key")
	}

	return publicKey, nil
}

// ReadECDSAPrivateKey loads the ECDSA private key from a file
func ReadECDSAPrivateKey(filename string) (*ecdsa.PrivateKey, error) {
	encodedPrivateKey, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	ecdsaPrivateKey, err := decodeECDSAPrivateKey(encodedPrivateKey)
	if err != nil {
		return nil, err
	}
	return ecdsaPrivateKey, nil
}

// ReadECDSAPublicKey loads the ECDSA public key from a file
func ReadECDSAPublicKey(filename string) (*ecdsa.PublicKey, error) {
	encodedPublicKey, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	ecdsaPublicKey, err := DecodeECDSAPublicKeyFromPEM(encodedPublicKey)
	if err != nil {
		return nil, err
	}
	return ecdsaPublicKey, nil
}

// WriteECDSAKey writes the ECDSA private and public keys to files
// The file names are the same as the private key file name with the suffixes ".pub" and ".priv"
func WriteECDSAKeys(filename string, privateKey *ecdsa.PrivateKey) error {
	pemPrivateKey, pemPublicKey, err := ecdsaEncodeToPEM(privateKey)
	if err != nil {
		return err
	}
	err = os.WriteFile(filename+".priv", []byte(pemPrivateKey), 0600)
	if err != nil {
		return err
	}
	err = os.WriteFile(filename+".pub", []byte(pemPublicKey), 0644)
	if err != nil {
		return err
	}
	return nil
}
