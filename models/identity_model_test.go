package custodian

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"testing"
)

func TestGetKID(t *testing.T) {
	curve := elliptic.P256()
	var privateKey1 = new(ecdsa.PrivateKey)
	var privateKey2 = new(ecdsa.PrivateKey)
	privateKey1, err := ecdsa.GenerateKey(curve, rand.Reader)
	if err != nil {
		t.Error("Failed to generate ECDSA private key.")
	}
	privateKey2, err = ecdsa.GenerateKey(curve, rand.Reader)
	if err != nil {
		t.Error("Failed to generate ECDSA private key.")
	}

	identity1 := Identity{
		ID:         "bob",
		PublicKey:  &privateKey1.PublicKey,
		PrivateKey: privateKey1,
	}

	identity2 := Identity{
		ID:         "alice",
		PublicKey:  &privateKey2.PublicKey,
		PrivateKey: privateKey2,
	}

	kid1_t1 := identity1.GetKID()
	if kid1_t1 == "" {
		t.Error("Failed to get KID 1 t1")
	}

	kid1_t2 := identity1.GetKID()
	if kid1_t2 == "" {
		t.Error("Failed to get KID 1 t2")
	}

	if kid1_t1 != kid1_t2 {
		t.Error("KID is not same for the same identity")
	}

	kid2_t1 := identity2.GetKID()
	if kid2_t1 == "" {
		t.Error("Failed to get KID 2")
	}

	if kid1_t1 == kid2_t1 {
		t.Error("KID is same for different identities")
	}
}
