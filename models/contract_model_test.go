package custodian

import (
	"encoding/json"
	"testing"
)

const (
	contractJSON  string = `{"id":"did:custodian:MyContract","tag":[{"name":"clientid","value":"custodian-client"}],"contract":[{"lang":"en","title":"End User Agreement","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","check":"I agree to the terms of the service","action":["accept"]},{"lang":"ch-fr","title":"Contrat d'utilisateur","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","check":"J'accepte les conditions générales d'utilisation","action":["accepter"]}],"var":[{"id":"did:custodian:MyContract#userid","value":"","type":"id","regexp":"[0-9a-z:_-]+"},{"id":"did:custodian:MyContract#controllerid","value":"MontreuxJazz","type":"","regexp":""}],"subject":[{"id":"did:custodian:MyContract#subj-1","attribute":{"id":"#userid","sign":["#id"]}},{"id":"did:custodian:MyContract#subj-2","attribute":{"id":"#controllerid","sign":["#id"]}}],"verb":[{"id":"did:custodian:MyContract#deploy","function":"add","attribute":{}}],"object":[{"id":"did:custodian:MyContract#obj-1","owner":"#subj-2","attribute":{}}],"scope":[{"subject":["#subj-1","#subj-2"],"verb":["#deploy"],"object":["#obj-1"]}],"validity":{"notBefore":"2021-01-01T00:00:00Z","notAfter":"2022-12-01T23:59:59Z","sign":["#userid","#controllerid"]}}`
	signatureJSON string = `{"id":"did:custodian:MySignature","header":{"body":{"id":"did:custodian:MyContract","digestMethod":"SHA256base64","digest":"FnYchy+BFVC75D8WZxcI95FMNxEXmyHOqIwSp+PFswo="},"prev":{}},"seal":[]}`
)

func TestTagString(t *testing.T) {
	name := "name"
	value := "value"

	tag := Tag{name, value}
	got := tag.String()
	want := `{"name":"` + name + `","value":"` + value + `"}`

	if got != want {
		t.Fatalf(`tag.String() = %q, want "%s"`, got, want)
	}
}

func TestContractFunctions(t *testing.T) {
	var contractBody ContractBody
	wantOID := "did:custodian:MyContract"
	err := json.Unmarshal([]byte(contractJSON), &contractBody)
	if err != nil {
		t.Fatalf(`Could not unmarshal contract body.`)
	}
	if contractBody.GetOID() != wantOID {
		t.Fatalf(`contractBody.GetOID() = %q, want "%s"`, contractBody.GetOID(), wantOID)
	}
}

func TestSignatureFunctions(t *testing.T) {
	var signature Signature

	wantOID := "did:custodian:MySignature"
	wantSignature := Signature{"did:custodian:MySignature", SignatureHeader{Link{"did:custodian:MyContract", "SHA256base64", "FnYchy+BFVC75D8WZxcI95FMNxEXmyHOqIwSp+PFswo="}, Link{}}, []Seal{}}

	wantHash, err := wantSignature.Hash()
	if err != nil {
		t.Fatalf(`wantSignature.Hash(): %v`, err)
	} else if wantHash == nil {
		t.Fatal("wantSignature.Hash() is nil")
	}

	err = json.Unmarshal([]byte(signatureJSON), &signature)
	if err != nil {
		t.Fatalf(`Could not unmarshal contract body.`)
	}
	if signature.GetOID() != wantOID {
		t.Fatalf(`contractBody.GetOID() = %q, want "%s"`, signature.GetOID(), wantOID)
	}

	hash, err := signature.Hash()
	if err != nil {
		t.Fatalf(`signature.Hash(): %v`, err)
	} else if hash == nil || string(hash) != string(wantHash) {
		t.Fatalf(`signature.Hash() = %q, want "%q"`, hash, wantHash)
	}
}
