/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package custodian

import (
	"crypto/sha256"
	"encoding/asn1"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

// # Structures

// ContractBody is the structure of the contracts used by the Custodian
type ContractBody struct {
	OID      string                `json:"id,omitempty" bson:"id"`
	Tag      []Tag                 `json:"tag" bson:"tag"`
	Contract []ContractDescription `json:"contract" bson"contract"`
	Var      []Variable            `json:"var" bson:"var"`
	Subject  []Subject             `json:"subject" bson:"subject"`
	Verb     []Verb                `json:"verb" bson:"verb"`
	Object   []Object              `json:"object" bson:"object"`
	Scope    []Scope               `json:"scope" bson:"scope"`
	Validity Validity              `json:"validity" bson:"validity"`
}

type Tag struct {
	Name  string `json:"name" bson:"name"`
	Value string `json:"value" bson:"value"`
}

type ContractDescription struct {
	Language    string   `json:"lang" bson:"lang"`
	Title       string   `json:"title" bson:"title"`
	Description string   `json:"description" bson:"description"` // Human readable
	Check       string   `json:"check" bson:"check"`
	Action      []string `json:"action" bson:"action"`
}

type Variable struct {
	ID     string `json:"id" bson:"id"`
	Value  string `json:"value" bson:"value"`
	Type   string `json:"type" bson:"type"`
	Regexp string `json:"regexp" bson:"regexp"`
}

type Subject struct {
	ID        string                 `json:"id" bson:"id"`
	Attribute map[string]interface{} `json:"attribute" bson:"attribute"`
}

type Verb struct {
	ID        string                 `json:"id" bson:"id"`
	Function  string                 `json:"function" bson:"function"`
	Attribute map[string]interface{} `json:"attribute" bson:"attribute"`
}

type Object struct {
	ID        string                 `json:"id" bson:"id"`
	Owner     string                 `json:"owner" bson:"owner"`
	Attribute map[string]interface{} `json:"attribute" bson:"attribute"`
}

type Scope struct {
	Subject []string `json:"subject" bson:"subject"`
	Verb    []string `json:"verb" bson:"verb"`
	Object  []string `json:"object" bson:"object"`
}

type Validity struct {
	NotBefore time.Time `json:"notBefore" bson:"notBefore"` // Contract valid from
	NotAfter  time.Time `json:"notAfter" bson:"notAfter"`   // Contract valid until
	Sign      []string  `json:"sign" bson:"sign"`
}

// Signature is the structure of the contract signature used by the Custodian.
//
// When a contract requires multiple signatures, the latter are chained using reference links in their header.
type Signature struct {
	OID    string          `json:"id" bson:"id" asn1:"printable"`
	Header SignatureHeader `json:"header" bson:"header"`
	Seal   []Seal          `json:"seal" bson:"seal"`
}

// SignatureHeader represents the header of the signatures
//
// The header contains a link to the contract body it makes reference to, as well as a link to the previous signature in the chain. If the signature is the first of the chain, the Prev field is empty.
type SignatureHeader struct {
	Body Link `json:"body" bson:"body" asn1:"omitempty"`
	Prev Link `json:"prev" bson:"prev" asn1:"omitempty,optional"`
}

// Seal is the structure used to store the users digital signatures and their information
type Seal struct {
	Signee          string    `json:"signee" bson:"signee" asn1:"printable"`
	Timestamp       time.Time `json:"timestamp" bson:"timestamp" asn1:"generalized"`
	Source          string    `json:"source" bson:"source" asn1:"source"`
	SignatureMethod string    `json:"signatureMethod" bson:"signatureMethod" asn1:"printable"`
	Signature       string    `json:"signature" bson:"signature" asn1:"printable,optional"`
}

// Link is used to make a reference to a contract or signature.
//
// The digest attribute contains a base4 hash of the contract/signature the link points to.
type Link struct {
	ID           string `json:"id" bson:"id" asn1:"printable"` // FIXME: OID?
	DigestMethod string `json:"digestMethod" bson:"digestMethod" asn1:"printable"`
	Digest       string `json:"digest" bson:"digest" asn1:"printable"`
}

// ContractString is used to store a contract as a string in the database
type ContractString struct {
	OID      string `json:"id" bson:"id" asn1:"printable"`
	Contract string `json:"contract" bson:"contract" asn1:"printable"`
}

// # Functions and Methods

// TimeIn returns the time t encoded for a specific zone (e.g., "UTC")
func TimeIn(t time.Time, zone string) time.Time {
	loc := time.FixedZone(zone, 0)
	return t.In(loc)
}

func (b ContractBody) GetOID() string {
	return b.OID
}

func (b *ContractBody) SetOID(oid string) {
	b.OID = oid
}

func (b ContractBody) String() string {
	//b.OID = ""
	str, err := json.Marshal(b)
	if err != nil {
		str = []byte("{}") // FIXME: safe?
	}

	return string(str)
}

func (b ContractBody) Hash() []byte {
	h := sha256.New()

	str := fmt.Sprintf("%v", b)
	h.Write([]byte(str))
	contractHash := h.Sum(nil)

	return contractHash
}

func (t Tag) String() string {
	str := "{"
	str += fmt.Sprintf("\"name\":\"%s\",", t.Name)
	str += fmt.Sprintf("\"value\":\"%s\"", t.Value)
	str += "}"

	return str
}

func (c ContractDescription) String() string {
	str := "{"
	str += fmt.Sprintf("\"lang\":\"%s\",", c.Language)
	str += fmt.Sprintf("\"title\":\"%s\",", c.Title)
	str += fmt.Sprintf("\"description\":\"%s\",", c.Description)
	str += fmt.Sprintf("\"check\":\"%s\",", c.Check)
	str += fmt.Sprintf("\"action\":%s", c.Action)
	str += "}"

	return str
}

func (v Variable) String() string {
	str := "{"
	str += fmt.Sprintf("\"id\":\"%s\",", v.ID)
	str += fmt.Sprintf("\"value\":\"%s\",", v.Value)
	str += fmt.Sprintf("\"type\":\"%s\",", v.Type)
	str += fmt.Sprintf("\"regexp\":\"%s\"", v.Regexp)
	str += "}"

	return str
}
func (s Subject) String() string {
	str := "{"
	str += fmt.Sprintf("\"id\":\"%s\",", s.ID)
	str += fmt.Sprintf("\"attribute\":\"%s\"", s.Attribute)
	str += "}"

	return str
}
func (v Verb) String() string {
	str := "{"
	str += fmt.Sprintf("\"id\":\"%s\",", v.ID)
	str += fmt.Sprintf("\"function\":\"%s\"", v.Function)
	str += fmt.Sprintf("\"attribute\":\"%s\"", v.Attribute)
	str += "}"

	return str
}
func (o Object) String() string {
	str := "{"
	str += fmt.Sprintf("\"id\":\"%s\",", o.ID)
	str += fmt.Sprintf("\"owner\":\"%s\",", o.Owner)
	str += fmt.Sprintf("\"attribute\":\"%s\"", o.Attribute)
	str += "}"

	return str
}
func (s Scope) String() string {
	str := "{"
	str += fmt.Sprintf("\"subject\":%s,", s.Subject)
	str += fmt.Sprintf("\"verb\":%s,", s.Verb)
	str += fmt.Sprintf("\"object\":%s", s.Object)
	str += "}"

	return str
}
func (v Validity) String() string {
	str := "{"
	str += fmt.Sprintf("\"notBefore\":\"%s\",", v.NotBefore)
	str += fmt.Sprintf("\"notAfter\":\"%s\",", v.NotAfter)
	str += fmt.Sprintf("\"sign\":%s", v.Sign) //FIXME: list not printed correctly
	str += "}"

	return str
}

func (s Signature) GetOID() string {
	return s.OID
}

func (s *Signature) SetOID(oid string) {
	s.OID = oid
}

func (s Signature) String() string {
	return s.PartialString(len(s.Seal))
}

func (s Signature) PartialString(index int) string {
	if index > len(s.Seal) {
		index = len(s.Seal)
	}

	str := "{"
	str += fmt.Sprintf("\"id\":\"%s\",", s.OID) // FIXME: Should it be included?
	str += fmt.Sprintf("\"header\":%s,", s.Header.String())
	str += "\"seal\":["
	for seal_i, seal := range s.Seal[:index] {
		if seal_i < index-1 {
			str += seal.String() + ","
		} else {
			str += seal.String()
		}
	}
	str += "]}"

	return str
}

func (s Signature) ComputeOID(timestamp time.Time, signee string) string {
	h := sha256.New()

	str := fmt.Sprintf("%s", s.Header.String())
	h.Write([]byte(str))
	contractHash := h.Sum(nil)

	formatted_timestamp := TimeIn(timestamp.Round(time.Millisecond), "UTC").Format(time.RFC3339) // FIXME: should we use something else than RFC3339 ?
	str = fmt.Sprintf("%s:%s:%s", contractHash, formatted_timestamp, signee)
	h = sha256.New()
	h.Write([]byte(str))
	contractHash = h.Sum(nil)

	dst := make([]byte, hex.EncodedLen(len(contractHash)))
	hex.Encode(dst, contractHash)

	return fmt.Sprintf("%s", dst)

}

// FormatTimestamps converts the seals timestamps to UTC
func (s *Signature) FormatTimestamps() {
	for seal_i, seal := range s.Seal {
		s.Seal[seal_i].Timestamp = TimeIn(seal.Timestamp.Round(time.Millisecond), "UTC")
	}
}

// Hash returns a hash of the signature
func (s Signature) Hash() ([]byte, error) {
	signatureASN1, err := s.ToASN1()
	if err != nil {
		return []byte{}, err
	}

	h := sha256.New()
	h.Write([]byte(signatureASN1))
	contractHash := h.Sum(nil)

	return contractHash, nil
}

func (h SignatureHeader) String() string {
	str := "{"
	str += fmt.Sprintf("\"body\":{\"id\":\"%s\",\"digestMethod\":\"%s\",\"digest\":\"%s\"}", h.Body.ID, h.Body.DigestMethod, h.Body.Digest)
	if h.Prev.ID != "" {
		str += fmt.Sprintf(",\"prev\":{\"id\":\"%s\",\"digestMethod\":\"%s\",\"digest\":\"%s\"}", h.Prev.ID, h.Prev.DigestMethod, h.Prev.Digest)
	} else {
		str += ",\"prev\":{}"
	}
	str += "}"

	return str
}

func (s Seal) String() string {
	str := "{"
	str += fmt.Sprintf("\"signee\":\"%s\",", s.Signee)
	str += fmt.Sprintf("\"timestamp\":\"%s\",", TimeIn(s.Timestamp.Round(time.Millisecond), "UTC").Format(time.RFC3339))
	str += fmt.Sprintf("\"source\":\"%s\",", s.Source)
	str += fmt.Sprintf("\"signatureMethod\":\"%s\",", s.SignatureMethod)
	str += fmt.Sprintf("\"signature\":\"%s\"", s.Signature)
	str += "}"

	return str
}

func (s Signature) ToASN1() ([]byte, error) {
	s.FormatTimestamps() // To make sure we always the same format

	signatureASN1, err := asn1.Marshal(s)
	if err != nil {
		return nil, err
	}
	return signatureASN1, nil
}

// PreSignatureHash returns the hash of the signature before we added the last seal's signature attribute
func (s Signature) PreSignatureHash() ([]byte, error) {
	if len(s.Seal) == 0 {
		return []byte{}, errors.New("Missing Seal")
	}

	last_index := len(s.Seal) - 1

	// Deep copy of s.Seal
	newSeal := make([]Seal, len(s.Seal))
	copy(newSeal, s.Seal)
	newSeal[last_index].Signature = ""

	s.Seal = newSeal

	return s.Hash()
}

func (c ContractString) GetOID() string {
	return c.OID
}

func (c *ContractString) SetOID(oid string) {
	c.OID = oid
}

func (c ContractString) String() string {
	return c.Contract
}

func (c ContractString) Hash() []byte {
	h := sha256.New()

	h.Write([]byte(c.Contract))
	contractHash := h.Sum(nil)

	return contractHash
}
