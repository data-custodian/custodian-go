# Interfaces and Structures

There is currently one interface (Asset, defined in `custodian/asset_model.go`) that represents items stored in the Custodian.

The Asset interface has three functions:
* `GetOID() string`: returns the OID of the Asset
* `SetOID(OID string)`: modifies the OID of the Asset
* `String() string`: returns a printable version of the Asset

The structures for the contract and the signature are defined in `custodian/model_contract.go`.

## Documentation

To read the documentation you must install `godoc`. Open a terminal in the `custodian/src/core/common` folder or one of its subfolders, and run the following command:

```
godoc -http=localhost:6060
```

The documentation for the structures and interfaces is accessible at [http://localhost:6060/pkg/gitlab.com/data-custodian/custodian-go/models/](http://localhost:6060/pkg/gitlab.com/data-custodian/custodian-go/models/)
