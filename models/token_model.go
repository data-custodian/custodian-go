package custodian

import "github.com/golang-jwt/jwt"

type TokenClaims struct {
	UserID      string       `json:"userid"`
	ClientID    string       `json:"clientid"`
	SignatureID string       `json:"signatureid"`
	Actor       string       `json:"actor"`
	Scope       []string     `json:"scope"`
	Constraints []Constraint `json:"constraints"`
	jwt.StandardClaims
}

type Constraint struct {
	Function             string                 `json:"function"`
	Owner                string                 `json:"owner"`
	ConstraintAttributes map[string]interface{} `json:"constraint_attributes"`
}
