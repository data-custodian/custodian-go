package custodian

type Event struct {
	Unix      int64                  `json:"u"`
	Token     string                 `json:"token"`
	ID        string                 `json:"id"`
	Resource  string                 `json:"resource"`
	Action    string                 `json:"action"`
	Attribute map[string]interface{} `json:"attribute"`
}
