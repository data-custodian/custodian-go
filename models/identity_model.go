package custodian

import (
	"crypto/ecdsa"
	"crypto/sha256"
	"crypto/x509"
	b64 "encoding/base64"
)

type Identity struct {
	ID         string           `json:"id"`
	PublicKey  *ecdsa.PublicKey `json:"publicKey"`
	PrivateKey *ecdsa.PrivateKey
}

func (id Identity) GetKID() string {
	x509EncodedPub, err := x509.MarshalPKIXPublicKey(id.PublicKey)
	if err != nil {
		return ""
	}

	h := sha256.New()
	h.Write(x509EncodedPub)
	hash := h.Sum(nil)
	return b64.URLEncoding.EncodeToString(hash)
}
